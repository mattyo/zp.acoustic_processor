{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"openrect" : [ 2.0, 45.0, 1200.0, 755.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 9.0,
		"default_fontface" : 0,
		"default_fontname" : "Monaco",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 15,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 1200.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "monacy",
		"subpatcher_template" : "",
		"globalpatchername" : "top",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-58",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 700.5, 607.0, 178.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 992.5, 613.0, 82.0, 22.0 ],
					"style" : "",
					"text" : "CPU usage"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 684.0, 643.0, 75.0, 20.0 ],
					"style" : "",
					"text" : "sprintf %ld%"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor2" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.784314, 0.145098, 0.023529, 0.0 ],
					"bgfillcolor_color1" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"fontname" : "Arial",
					"fontsize" : 24.0,
					"gradient" : 1,
					"id" : "obj-51",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 684.0, 677.0, 70.0, 35.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1001.5, 640.0, 64.0, 35.0 ],
					"style" : "",
					"text" : "0%",
					"textcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 684.0, 583.0, 100.0, 31.0 ],
					"style" : "",
					"text" : "qmetro 200 @active 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 684.0, 617.0, 100.0, 20.0 ],
					"style" : "",
					"text" : "adstatus cpu"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "bend" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-42",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "benderEngine~.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 577.166687, 251.0, 153.0, 54.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 11.0, 439.0, 895.0, 116.0 ],
					"varname" : "benderEngine~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-46",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 436.0, 526.0, 178.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 362.5, 586.0, 126.0, 22.0 ],
					"style" : "",
					"text" : "record test sounds"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-45",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 421.0, 511.0, 178.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 199.0, 602.0, 122.0, 22.0 ],
					"style" : "",
					"text" : "record master out"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Verdana",
					"fontsize" : 12.0,
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 593.0, 89.0, 669.0, 595.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Verdana",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Verdana",
									"fontsize" : 12.0,
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 58.0, 307.0, 113.0, 21.0 ],
									"style" : "",
									"text" : "hours — minutes"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Verdana",
									"fontsize" : 12.0,
									"id" : "obj-7",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 44.0, 231.0, 127.0, 21.0 ],
									"style" : "",
									"text" : "minutes — seconds"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Verdana",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 15.0, 158.0, 156.0, 21.0 ],
									"style" : "",
									"text" : "seconds — milleseconds"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Verdana",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 196.0, 476.0, 100.0, 21.0 ],
									"style" : "",
									"text" : "time string out"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Verdana",
									"fontsize" : 12.0,
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 196.0, 31.0, 44.0, 21.0 ],
									"style" : "",
									"text" : "ms in"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678,
									"id" : "obj-97",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 171.0, 307.0, 34.0, 24.0 ],
									"style" : "",
									"text" : "/ 60"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678,
									"id" : "obj-98",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 276.666656, 307.0, 43.0, 24.0 ],
									"style" : "",
									"text" : "% 60"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678,
									"id" : "obj-96",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 171.0, 231.0, 34.0, 24.0 ],
									"style" : "",
									"text" : "/ 60"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678,
									"id" : "obj-79",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 382.333344, 232.0, 43.0, 24.0 ],
									"style" : "",
									"text" : "% 60"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Verdana",
									"fontsize" : 12.0,
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 171.0, 376.0, 336.0, 23.0 ],
									"style" : "",
									"text" : "sprintf set %i:%02i:%02i:%03i"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.733333, 0.815686, 0.627451, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 14.321678,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 171.0, 158.0, 51.0, 24.0 ],
									"style" : "",
									"text" : "/ 1000"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.733333, 0.815686, 0.627451, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 14.321678,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 488.0, 158.0, 60.0, 24.0 ],
									"style" : "",
									"text" : "% 1000"
								}

							}
, 							{
								"box" : 								{
									"comment" : "ms in",
									"id" : "obj-99",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 171.0, 31.0, 25.0, 25.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "time string out",
									"id" : "obj-100",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 171.0, 474.0, 25.0, 25.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 3 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 2 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-97", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-98", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-99", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-99", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 485.0, 640.0, 120.0, 23.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"fontname" : "Verdana",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p ms_to_h.n.s.ms"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 485.0, 611.0, 100.0, 20.0 ],
					"style" : "",
					"text" : "clocker 100"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor2" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"fontname" : "Arial",
					"fontsize" : 24.0,
					"gradient" : 1,
					"id" : "obj-53",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 485.0, 677.0, 159.0, 35.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 190.0, 701.0, 140.0, 35.0 ],
					"style" : "",
					"text" : "0:00:09:048",
					"textcolor" : [ 0.0, 1.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.720698, 0.16723, 0.080014, 1.0 ],
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 318.0, 329.0, 698.0, 419.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 366.0, 178.0, 22.0 ],
									"restore" : [ 10 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht15 @bindto ::hi::time[15]",
									"varname" : "ht15"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 342.0, 178.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht14 @bindto ::hi::time[14]",
									"varname" : "ht14"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 318.0, 178.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht13 @bindto ::hi::time[13]",
									"varname" : "ht13"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 294.0, 178.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht12 @bindto ::hi::time[12]",
									"varname" : "ht12"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 270.0, 176.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht11 @bindto ::hi::time[11]",
									"varname" : "ht11"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-71",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 246.0, 178.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht10 @bindto ::hi::time[10]",
									"varname" : "ht10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 222.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht9 @bindto ::hi::time[9]",
									"varname" : "ht9"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 198.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht8 @bindto ::hi::time[8]",
									"varname" : "ht8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 174.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht7 @bindto ::hi::time[7]",
									"varname" : "ht7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 150.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht6 @bindto ::hi::time[6]",
									"varname" : "ht6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 126.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht5 @bindto ::hi::time[5]",
									"varname" : "ht5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 102.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht4 @bindto ::hi::time[4]",
									"varname" : "ht4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 78.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht3 @bindto ::hi::time[3]",
									"varname" : "ht3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-79",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 54.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht2 @bindto ::hi::time[2]",
									"varname" : "ht2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 30.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht1 @bindto ::hi::time[1]",
									"varname" : "ht1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 403.0, 6.0, 165.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ht0 @bindto ::hi::time[0]",
									"varname" : "ht0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 366.0, 177.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st15 @bindto ::si::time[15]",
									"varname" : "st15"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 342.0, 177.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st14 @bindto ::si::time[14]",
									"varname" : "st14"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 318.0, 177.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st13 @bindto ::si::time[13]",
									"varname" : "st13"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 294.0, 177.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st12 @bindto ::si::time[12]",
									"varname" : "st12"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 270.0, 175.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st11 @bindto ::si::time[11]",
									"varname" : "st11"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 246.0, 177.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st10 @bindto ::si::time[10]",
									"varname" : "st10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 222.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st9 @bindto ::si::time[9]",
									"varname" : "st9"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 198.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st8 @bindto ::si::time[8]",
									"varname" : "st8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 174.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st7 @bindto ::si::time[7]",
									"varname" : "st7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 150.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st6 @bindto ::si::time[6]",
									"varname" : "st6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 126.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st5 @bindto ::si::time[5]",
									"varname" : "st5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 102.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st4 @bindto ::si::time[4]",
									"varname" : "st4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 78.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st3 @bindto ::si::time[3]",
									"varname" : "st3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 54.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st2 @bindto ::si::time[2]",
									"varname" : "st2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 30.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st1 @bindto ::si::time[1]",
									"varname" : "st1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 198.0, 6.0, 163.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr st0 @bindto ::si::time[0]",
									"varname" : "st0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 366.0, 171.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft15 @bindto ::fi::time[15]",
									"varname" : "ft15"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 342.0, 171.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft14 @bindto ::fi::time[14]",
									"varname" : "ft14"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 318.0, 171.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft13 @bindto ::fi::time[13]",
									"varname" : "ft13"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 294.0, 171.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft12 @bindto ::fi::time[12]",
									"varname" : "ft12"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 270.0, 170.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft11 @bindto ::fi::time[11]",
									"varname" : "ft11"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 246.0, 171.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft10 @bindto ::fi::time[10]",
									"varname" : "ft10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 222.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft9 @bindto ::fi::time[9]",
									"varname" : "ft9"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 198.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft8 @bindto ::fi::time[8]",
									"varname" : "ft8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 174.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft7 @bindto ::fi::time[7]",
									"varname" : "ft7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 150.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft6 @bindto ::fi::time[6]",
									"varname" : "ft6"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 126.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft5 @bindto ::fi::time[5]",
									"varname" : "ft5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 102.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft4 @bindto ::fi::time[4]",
									"varname" : "ft4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 78.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft3 @bindto ::fi::time[3]",
									"varname" : "ft3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 54.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft2 @bindto ::fi::time[2]",
									"varname" : "ft2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 30.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft1 @bindto ::fi::time[1]",
									"varname" : "ft1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 6.0, 6.0, 158.0, 22.0 ],
									"restore" : [ 0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr ft0 @bindto ::fi::time[0]",
									"varname" : "ft0"
								}

							}
 ],
						"lines" : [  ]
					}
,
					"patching_rect" : [ 923.0, 631.0, 100.0, 20.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p static-storage",
					"varname" : "foo"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 810.0, 631.0, 100.0, 20.0 ],
					"style" : "",
					"text" : "pattrmarker top"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-44",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 321.0, 583.0, 100.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 190.0, 652.0, 140.0, 42.0 ],
					"style" : "",
					"text" : "Start Recording",
					"textcolor" : [ 0.439216, 0.74902, 0.254902, 1.0 ],
					"texton" : "Stop Recording",
					"textoncolor" : [ 1.0, 0.23254, 0.108888, 1.0 ],
					"varname" : "master-record"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 255.0, 572.0, 32.0, 20.0 ],
					"style" : "",
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-40",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 255.0, 525.0, 100.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 190.0, 628.0, 140.0, 20.0 ],
					"style" : "",
					"text" : "Open new file",
					"varname" : "record-file-picker"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 255.0, 626.0, 70.0, 20.0 ],
					"style" : "",
					"text" : "sfrecord~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 516.0, 443.5, 50.0, 31.0 ],
					"style" : "",
					"text" : "window getsize"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 441.0, 542.0, 130.0, 31.0 ],
					"style" : "",
					"text" : "window size 413 46 1613 801"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 516.0, 499.0, 100.0, 20.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.345098, 0.513725, 0.572549, 1.0 ],
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 496.5, 102.0, 75.0, 20.0 ],
					"style" : "",
					"text" : "r record"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.345098, 0.513725, 0.572549, 1.0 ],
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 372.0, 102.0, 75.0, 20.0 ],
					"style" : "",
					"text" : "r start-stop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1019.0, 200.0, 48.0, 20.0 ],
					"style" : "",
					"text" : "del 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.0, 272.0, 43.0, 20.0 ],
					"style" : "",
					"text" : "wclose"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1019.0, 247.0, 54.0, 20.0 ],
					"style" : "",
					"text" : "del 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1093.0, 260.0, 38.0, 20.0 ],
					"style" : "",
					"text" : "front"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 1019.0, 222.0, 93.0, 20.0 ],
					"style" : "",
					"text" : "b 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1019.0, 178.0, 54.0, 20.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 43.0, 10.5, 65.0, 20.0 ],
					"style" : "",
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"rect" : [ 780.0, 263.0, 866.0, 639.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 384.0, 491.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 287.0, 493.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 50.0, 195.0, 100.0, 22.0 ],
									"style" : "",
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 289.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "pause"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 287.0, 316.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 287.0, 392.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "start 0 $1 $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 362.0, 100.0, 52.0, 22.0 ],
									"style" : "",
									"text" : "clear"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 269.0, 276.0, 54.0, 22.0 ],
									"style" : "",
									"text" : "* 30000."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 269.0, 250.0, 65.0, 22.0 ],
									"style" : "",
									"text" : "snapshot~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 100.0, 88.0, 100.0, 22.0 ],
									"style" : "",
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 181.0, 117.0, 48.0, 22.0 ],
									"style" : "",
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 287.0, 430.0, 100.0, 22.0 ],
									"style" : "",
									"text" : "play~ tmpbuf"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "bang" ],
									"patching_rect" : [ 437.0, 134.5, 123.0, 22.0 ],
									"style" : "",
									"text" : "buffer~ tmpbuf 30000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 266.0, 181.0, 100.0, 22.0 ],
									"style" : "",
									"text" : "record~ tmpbuf"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 437.0, 100.0, 52.0, 22.0 ],
									"style" : "",
									"text" : "write"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-32",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 100.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-34",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 266.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-38",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 437.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-30", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-35", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 348.5, 134.0, 425.0, 20.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p rec-and-play"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1050.5, 509.5, 111.0, 20.0 ],
					"style" : "",
					"text" : "loadmess set tmpbuf"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"id" : "obj-56",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 348.5, 51.0, 67.0, 31.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 501.0, 701.0, 67.0, 34.0 ],
					"style" : "",
					"text" : "Play",
					"textcolor" : [ 0.8, 1.0, 0.4, 1.0 ],
					"texton" : "Stop ",
					"textoncolor" : [ 1.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"allowdrag" : 0,
					"annotation" : "Select a portion of sound to play - extend the selection using the shift key.",
					"bgcolor" : [ 0.094118, 0.113725, 0.137255, 0.0 ],
					"buffername" : "tmpbuf",
					"fontsize" : 9.0,
					"gridcolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"id" : "obj-33",
					"ignoreclick" : 1,
					"invert" : 1,
					"labels" : 0,
					"maxclass" : "waveform~",
					"numinlets" : 5,
					"numoutlets" : 6,
					"outlettype" : [ "float", "float", "float", "float", "list", "" ],
					"patching_rect" : [ 1050.5, 543.5, 123.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 354.875, 701.0, 128.75, 28.0 ],
					"prototypename" : "M4L.orange",
					"ruler" : 0,
					"selectioncolor" : [ 0.301961, 0.337255, 0.403922, 0.501961 ],
					"setmode" : 1,
					"snapto" : 2,
					"style" : "",
					"vticks" : 0,
					"waveformcolor" : [ 0.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 0.0,
					"bgcolor" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"bordercolor" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"id" : "obj-71",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1042.5, 537.5, 136.0, 28.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 350.5, 694.0, 137.5, 42.0 ],
					"proportion" : 0.39,
					"prototypename" : "M4L.horizontal-black",
					"rounded" : 16,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"id" : "obj-29",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 754.5, 51.5, 135.0, 31.0 ],
					"style" : "",
					"text" : "Save Recording",
					"textcolor" : [ 0.8, 1.0, 0.4, 1.0 ],
					"texton" : "Stop Recording",
					"textoncolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"id" : "obj-27",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 436.5, 51.5, 135.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.0, 656.0, 135.0, 34.0 ],
					"style" : "",
					"text" : "Start Recording",
					"textcolor" : [ 0.8, 1.0, 0.4, 1.0 ],
					"texton" : "Stop Recording",
					"textoncolor" : [ 1.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"channels" : 1,
					"id" : "obj-25",
					"maxclass" : "live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 619.166687, 51.5, 86.0, 41.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.0, 613.0, 136.0, 41.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[3]",
							"parameter_shortname" : "rec level",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 619.166687, 20.5, 32.0, 20.0 ],
					"style" : "",
					"text" : "adc~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 43.0, 73.5, 29.5, 20.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 43.0, 102.0, 98.0, 20.0 ],
					"style" : "",
					"text" : "selector~ 2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"id" : "obj-10",
					"maxclass" : "tab",
					"multiline" : 0,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 43.0, 38.0, 156.0, 28.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 154.0, 8.0, 156.0, 28.0 ],
					"style" : "",
					"tabs" : [ "live", "recorded" ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.0, 424.5, 113.0, 20.0 ],
					"style" : "",
					"text" : "store 1, writeagain"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.239216, 0.643137, 0.709804, 1.0 ],
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1019.0, 394.5, 60.0, 20.0 ],
					"style" : "",
					"text" : "closebang",
					"varname" : "u134002368[5]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 810.0, 571.5, 50.0, 20.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 810.0, 543.5, 100.0, 20.0 ],
					"style" : "",
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 810.0, 517.5, 100.0, 20.0 ],
					"style" : "",
					"text" : "route read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"items" : [ "storagewindow", ",", "write", ",", "writeagain", ",", "read" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 810.0, 416.5, 100.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Monaco",
					"fontsize" : 9.0,
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 810.0, 443.5, 158.0, 20.0 ],
					"style" : "",
					"text" : "loadmess read zp.ap.storage"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Monaco",
					"fontsize" : 9.0,
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 930.0, 512.25, 60.0, 20.0 ],
					"restore" : 					{
						"input_gain" : [ 0.0 ],
						"live.gain~" : [ -0.221123 ],
						"live.gain~[1]" : [ -64.015747 ],
						"master-record" : [ 0 ],
						"raw_out" : [ -48.456692 ],
						"record-file-picker" : [ -1 ]
					}
,
					"style" : "",
					"text" : "autopattr",
					"varname" : "u803044521"
				}

			}
, 			{
				"box" : 				{
					"active" : 					{
						"master-record" : 0,
						"record-file-picker" : 0,
						"interfaces::freezer-interface::Record[4]" : 0,
						"interfaces::freezer-interface::go_button" : 0,
						"interfaces::freezer-interface::live.button[1]" : 0,
						"interfaces::freezer-interface::live.tab" : 0,
						"interfaces::freezer-interface::live.tab[6]" : 0,
						"interfaces::freezer-interface::live.text" : 0,
						"interfaces::freezer-interface::live.text[1]" : 0,
						"interfaces::freezer-interface::xfade_time" : 0,
						"interfaces::freezer-interface::rec" : 0,
						"interfaces::sogs-interface::live.button[1]" : 0,
						"interfaces::sogs-interface::live.tab" : 0,
						"interfaces::sogs-interface::live.tab[1]" : 0,
						"interfaces::sogs-interface::live.tab[5]" : 0,
						"interfaces::sogs-interface::live.tab[6]" : 0,
						"interfaces::sogs-interface::live.text" : 0,
						"interfaces::sogs-interface::live.text[1]" : 0,
						"interfaces::sogs-interface::rec" : 0,
						"interfaces::harm_interface::live.tab" : 0,
						"interfaces::harm_interface::live.tab[1]" : 0,
						"interfaces::harm_interface::live.tab[5]" : 0,
						"interfaces::harm_interface::live.tab[6]" : 0,
						"interfaces::harm_interface::live.text" : 0,
						"interfaces::harm_interface::live.text[1]" : 0,
						"interfaces::bend-interface::live.tab" : 0,
						"interfaces::bend-interface::live.tab[1]" : 0,
						"interfaces::bend-interface::live.tab[5]" : 0,
						"interfaces::bend-interface::live.tab[6]" : 0,
						"interfaces::bend-interface::live.text" : 0,
						"interfaces::bend-interface::live.text[1]" : 0
					}
,
					"fontface" : 0,
					"fontname" : "Monaco",
					"fontsize" : 9.0,
					"id" : "obj-64",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 810.0, 473.5, 213.0, 31.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 1831, -966, 2235, -90 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"style" : "",
					"text" : "pattrstorage zp.ap.storage @savemode 3 @fileusagemode 1 @autorestore 0",
					"varname" : "zp.ap.storage"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x86",
							"modernui" : 1
						}
,
						"openrect" : [ 113.0, 175.0, 366.0, 264.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 366.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patching_rect" : [ 195.5, 110.0, 100.0, 22.0 ],
									"style" : "",
									"text" : "bend-interface",
									"varname" : "bend-interface"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patching_rect" : [ 195.5, 27.0, 108.0, 22.0 ],
									"style" : "",
									"text" : "harm-interface",
									"varname" : "harm_interface"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patching_rect" : [ 195.5, 58.0, 108.0, 22.0 ],
									"style" : "",
									"text" : "sogs-interface",
									"varname" : "sogs-interface"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patching_rect" : [ 195.5, 84.0, 108.0, 22.0 ],
									"style" : "",
									"text" : "freezer-interface",
									"varname" : "freezer-interface"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 592.0, 117.0, 69.0, 22.0 ],
									"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
									"style" : "",
									"text" : "thispatcher"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 592.0, 76.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.239216, 0.643137, 0.709804, 1.0 ],
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 15.0, 110.0, 55.0, 20.0 ],
									"style" : "",
									"text" : "s to_psto",
									"varname" : "u134002368[5]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 15.0, 74.0, 65.0, 22.0 ],
									"style" : "",
									"text" : "closebang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 195.5, 169.0, 100.0, 22.0 ],
									"style" : "",
									"text" : "autopattr",
									"varname" : "u312044466"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "monacy",
								"default" : 								{
									"fontsize" : [ 9.0 ],
									"fontname" : [ "Monaco" ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1019.0, 300.0, 75.0, 20.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p interfaces",
					"varname" : "interfaces"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1026.5, 51.0, 32.0, 20.0 ],
					"style" : "",
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"id" : "obj-5",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1026.5, 22.0, 100.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 983.5, 586.0, 100.0, 20.0 ],
					"style" : "",
					"text" : "Audio Settings"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"orientation" : 1,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 43.0, 444.0, 136.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.0, 610.0, 136.0, 48.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[2]",
							"parameter_shortname" : "raw out level",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ -70.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "raw_out"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1026.5, 77.0, 45.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1011.0, 536.0, 45.0, 45.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 43.0, 596.0, 32.0, 20.0 ],
					"style" : "",
					"text" : "dac~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"orientation" : 1,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 43.0, 525.0, 136.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.0, 681.0, 136.0, 48.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[1]",
							"parameter_shortname" : "master",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "sogs" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-8",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "zp.sogs~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "" ],
					"patching_rect" : [ 230.0, 260.0, 204.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 11.0, 172.0, 1022.0, 148.0 ],
					"varname" : "zp.sogs~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "freeze" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-7",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "freezer-scrubber.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 448.5, 256.0, 115.0, 41.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 11.0, 322.0, 829.0, 115.0 ],
					"varname" : "freezer-scrubber",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "harm" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "zp.harm2.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 76.0, 256.0, 148.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 11.0, 51.0, 1098.0, 119.0 ],
					"varname" : "zp.harm",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"channels" : 1,
					"id" : "obj-3",
					"maxclass" : "live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 43.0, 140.0, 136.0, 41.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 11.0, 8.0, 136.0, 41.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~",
							"parameter_shortname" : "input",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "input_gain"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 82.5, 73.5, 32.0, 20.0 ],
					"style" : "",
					"text" : "adc~"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 819.5, 593.5, 797.0, 593.5, 797.0, 467.5, 819.5, 467.5 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-42", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-7::obj-38" : [ "scrub_rate", "Rate", 0 ],
			"obj-8::obj-64::obj-24" : [ "Selection", "Selection", 0 ],
			"obj-15::obj-12::obj-60" : [ "time[27]", "time[27]", 0 ],
			"obj-6::obj-68" : [ "Detune", "Detune", 0 ],
			"obj-8::obj-53" : [ "Clear", "Clear", 0 ],
			"obj-15::obj-12::obj-27" : [ "live.tab[22]", "live.tab[11]", 0 ],
			"obj-15::obj-3::obj-57" : [ "time[55]", "time[24]", 0 ],
			"obj-4" : [ "live.gain~[2]", "raw out level", 0 ],
			"obj-6::obj-48" : [ "SpecEnv", "EnvPres", 0 ],
			"obj-15::obj-12::obj-53" : [ "time[22]", "time[22]", 0 ],
			"obj-15::obj-12::obj-38" : [ "live.tab[15]", "live.tab[5]", 0 ],
			"obj-25" : [ "live.gain~[3]", "rec level", 0 ],
			"obj-6::obj-8::obj-18" : [ "EnvLpcOrder", "Order", 0 ],
			"obj-6::obj-66" : [ "live.text[1]", "live.text", 0 ],
			"obj-15::obj-12::obj-52" : [ "time[16]", "time[5]", 0 ],
			"obj-15::obj-3::obj-47" : [ "time[61]", "time[2]", 0 ],
			"obj-6::obj-8::obj-70" : [ "EnvTransp", "Transp", 0 ],
			"obj-7::obj-33::obj-43" : [ "live.dial[7]", "Fixed", 0 ],
			"obj-15::obj-11::obj-28" : [ "Record[4]", "Record", 0 ],
			"obj-15::obj-3::obj-42" : [ "live.tab[31]", "live.tab[5]", 0 ],
			"obj-6::obj-8::obj-43" : [ "PreserveTransients", "Transients", 1 ],
			"obj-8::obj-29::obj-43" : [ "live.dial[5]", "Fixed", 0 ],
			"obj-15::obj-11::obj-59" : [ "time[10]", "time[10]", 0 ],
			"obj-15::obj-12::obj-64" : [ "time[31]", "time[31]", 0 ],
			"obj-8::obj-90::obj-49" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-15::obj-11::obj-64" : [ "time[15]", "time[15]", 0 ],
			"obj-15::obj-13::obj-13" : [ "live.tab[19]", "live.tab[5]", 0 ],
			"obj-15::obj-3::obj-61" : [ "time[51]", "time[28]", 0 ],
			"obj-8::obj-55" : [ "Size", "Size", 0 ],
			"obj-15::obj-11::obj-52" : [ "time[5]", "time[5]", 0 ],
			"obj-15::obj-13::obj-64" : [ "time[32]", "time[31]", 0 ],
			"obj-42::obj-25" : [ "live.numbox[26]", "live.numbox", 0 ],
			"obj-6::obj-8::obj-78" : [ "PreserveWaveform", "Waveform", 1 ],
			"obj-6::obj-56::obj-49" : [ "live.numbox[10]", "live.numbox", 0 ],
			"obj-15::obj-13::obj-59" : [ "time[37]", "time[26]", 0 ],
			"obj-6::obj-8::obj-48" : [ "RemixRelax", "Relax", 0 ],
			"obj-7::obj-33::obj-49" : [ "live.numbox[4]", "live.numbox", 0 ],
			"obj-15::obj-13::obj-52" : [ "time[42]", "time[5]", 0 ],
			"obj-42::obj-107" : [ "live.numbox[16]", "live.numbox", 0 ],
			"obj-7::obj-4::obj-36" : [ "live.numbox[23]", "live.numbox", 0 ],
			"obj-15::obj-13::obj-21" : [ "time[47]", "time[20]", 0 ],
			"obj-7::obj-22" : [ "Delay[5]", "Delay", 0 ],
			"obj-15::obj-3::obj-59" : [ "time[53]", "time[26]", 0 ],
			"obj-6::obj-16" : [ "Dry/Wet", "Dry/Wet", 0 ],
			"obj-8::obj-93" : [ "Direction[1]", "Direction", 0 ],
			"obj-15::obj-12::obj-57" : [ "time[24]", "time[24]", 0 ],
			"obj-15::obj-12::obj-13" : [ "live.tab[16]", "live.tab[5]", 0 ],
			"obj-6::obj-45" : [ "Spread", "Spread", 0 ],
			"obj-8::obj-90::obj-43" : [ "live.dial[2]", "Fixed", 0 ],
			"obj-8::obj-91" : [ "PitchVar", "PitchVar", 0 ],
			"obj-15::obj-12::obj-5" : [ "live.button[3]", "live.button", 0 ],
			"obj-15::obj-3::obj-52" : [ "time[58]", "time[5]", 0 ],
			"obj-6::obj-8::obj-64" : [ "EnvMaxFreq", "Freq", 0 ],
			"obj-7::obj-18" : [ "live.text", "live.text", 0 ],
			"obj-8::obj-29::obj-36" : [ "live.numbox[22]", "live.numbox", 0 ],
			"obj-15::obj-12::obj-48" : [ "time[17]", "time[3]", 0 ],
			"obj-15::obj-3::obj-31" : [ "live.text[11]", "live.text[4]", 0 ],
			"obj-15::obj-3::obj-46" : [ "time[62]", "time[1]", 0 ],
			"obj-42::obj-52" : [ "live.gain~[4]", "Gain", 0 ],
			"obj-6::obj-8::obj-63" : [ "EnvTranspEnable", "Enable", 0 ],
			"obj-7::obj-68::obj-43" : [ "live.dial[8]", "Fixed", 0 ],
			"obj-15::obj-11::obj-60" : [ "time[11]", "time[11]", 0 ],
			"obj-15::obj-3::obj-63" : [ "time[49]", "time[30]", 0 ],
			"obj-8::obj-90::obj-36" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-15::obj-11::obj-46" : [ "time[1]", "time[1]", 0 ],
			"obj-15::obj-12::obj-61" : [ "time[28]", "time[28]", 0 ],
			"obj-15::obj-13::obj-55" : [ "live.tab[20]", "live.tab[11]", 0 ],
			"obj-6::obj-83" : [ "InitPitchTable", "Init", 0 ],
			"obj-8::obj-3" : [ "live.tab[4]", "live.tab", 0 ],
			"obj-8::obj-37" : [ "Record[1]", "Record", 0 ],
			"obj-15::obj-11::obj-13" : [ "live.tab[7]", "live.tab[5]", 0 ],
			"obj-15::obj-11::obj-53" : [ "time[6]", "time[6]", 0 ],
			"obj-15::obj-13::obj-63" : [ "time[33]", "time[30]", 0 ],
			"obj-42::obj-15" : [ "live.numbox[25]", "live.numbox", 0 ],
			"obj-42::obj-13" : [ "live.numbox[20]", "live.numbox", 0 ],
			"obj-6::obj-8::obj-54" : [ "RemixEnable", "Enable", 0 ],
			"obj-6::obj-56::obj-43" : [ "live.dial[3]", "Fixed", 0 ],
			"obj-7::obj-68::obj-36" : [ "live.numbox[7]", "live.numbox", 0 ],
			"obj-15::obj-12::obj-28" : [ "Record[2]", "Record", 0 ],
			"obj-15::obj-13::obj-58" : [ "time[38]", "time[25]", 0 ],
			"obj-6::obj-8::obj-51" : [ "RemixSinus", "Sinus", 0 ],
			"obj-7::obj-33::obj-28" : [ "live.tab[29]", "live.tab", 0 ],
			"obj-15::obj-13::obj-49" : [ "time[43]", "time[21]", 0 ],
			"obj-42::obj-4::obj-28" : [ "live.tab[37]", "live.tab", 0 ],
			"obj-42::obj-53" : [ "live.numbox[17]", "live.numbox", 0 ],
			"obj-7::obj-4::obj-43" : [ "live.dial[9]", "Fixed", 0 ],
			"obj-7::obj-17" : [ "Feedback[1]", "Feedback", 0 ],
			"obj-15::obj-12::obj-59" : [ "time[26]", "time[26]", 0 ],
			"obj-6::obj-7" : [ "Feedback", "Feedback", 0 ],
			"obj-8::obj-95" : [ "Gain", "Gain", 0 ],
			"obj-15::obj-12::obj-55" : [ "live.tab[17]", "live.tab[11]", 0 ],
			"obj-15::obj-3::obj-56" : [ "time[56]", "time[23]", 0 ],
			"obj-8::obj-30" : [ "Scrub[1]", "Scrub", 0 ],
			"obj-15::obj-3::obj-49" : [ "time[59]", "time[21]", 0 ],
			"obj-15::obj-3::obj-13" : [ "live.tab[35]", "live.tab[5]", 0 ],
			"obj-6::obj-8::obj-67" : [ "EnvMode", "Mode", 0 ],
			"obj-8::obj-29::obj-49" : [ "live.numbox[12]", "live.numbox", 0 ],
			"obj-15::obj-12::obj-47" : [ "time[18]", "time[2]", 0 ],
			"obj-15::obj-13::obj-40" : [ "live.text[4]", "live.text[4]", 0 ],
			"obj-15::obj-3::obj-21" : [ "time[63]", "time[20]", 0 ],
			"obj-6::obj-8::obj-45" : [ "FFTOversamp", "Oversampling", 0 ],
			"obj-6::obj-51::obj-49" : [ "live.numbox[1]", "live.numbox", 0 ],
			"obj-7::obj-14" : [ "Direction", "Direction", 0 ],
			"obj-15::obj-11::obj-61" : [ "time[12]", "time[12]", 0 ],
			"obj-15::obj-12::obj-63" : [ "time[30]", "time[30]", 0 ],
			"obj-8::obj-90::obj-28" : [ "live.tab[2]", "live.tab", 0 ],
			"obj-15::obj-11::obj-47" : [ "time[2]", "time[2]", 0 ],
			"obj-15::obj-11::obj-5" : [ "live.button[2]", "live.button", 0 ],
			"obj-15::obj-11::obj-56" : [ "time[7]", "time[7]", 0 ],
			"obj-15::obj-11::obj-55" : [ "live.tab[13]", "live.tab[11]", 0 ],
			"obj-15::obj-13::obj-62" : [ "time[34]", "time[29]", 0 ],
			"obj-42::obj-11" : [ "live.numbox[24]", "live.numbox", 0 ],
			"obj-42::obj-12" : [ "live.numbox[21]", "live.numbox", 0 ],
			"obj-6::obj-8::obj-47" : [ "RemixError", "Error", 0 ],
			"obj-6::obj-56::obj-36" : [ "live.numbox[11]", "live.numbox", 0 ],
			"obj-7::obj-68::obj-49" : [ "live.numbox[6]", "live.numbox", 0 ],
			"obj-15::obj-11::obj-38" : [ "live.tab[10]", "live.tab[5]", 0 ],
			"obj-15::obj-13::obj-57" : [ "time[39]", "time[24]", 0 ],
			"obj-6::obj-8::obj-49" : [ "RemixTransients", "Trans", 0 ],
			"obj-15::obj-13::obj-48" : [ "time[44]", "time[3]", 0 ],
			"obj-42::obj-4::obj-43" : [ "live.dial[4]", "Fixed", 0 ],
			"obj-42::obj-90" : [ "live.numbox[18]", "live.numbox", 0 ],
			"obj-7::obj-37" : [ "Record", "Record", 0 ],
			"obj-6::obj-76" : [ "AudioOutputGain", "Gain", 0 ],
			"obj-15::obj-12::obj-31" : [ "live.text[6]", "live.text[4]", 0 ],
			"obj-15::obj-3::obj-58" : [ "time[54]", "time[25]", 0 ],
			"obj-3" : [ "live.gain~", "input", 0 ],
			"obj-6::obj-51::obj-36" : [ "live.numbox", "live.numbox", 0 ],
			"obj-6::obj-53" : [ "PanSpread", "Spread", 0 ],
			"obj-8::obj-76" : [ "GrainSize", "GrainSize", 0 ],
			"obj-15::obj-12::obj-56" : [ "time[23]", "time[23]", 0 ],
			"obj-7::obj-7" : [ "live.slider", "Gain", 0 ],
			"obj-8::obj-67" : [ "SelectAll", "SelectAll", 0 ],
			"obj-15::obj-12::obj-49" : [ "time[21]", "time[21]", 0 ],
			"obj-15::obj-3::obj-55" : [ "live.tab[36]", "live.tab[11]", 0 ],
			"obj-6::obj-8::obj-61" : [ "EnvScale", "Scale", 0 ],
			"obj-8::obj-29::obj-28" : [ "live.tab[38]", "live.tab", 0 ],
			"obj-8::obj-61" : [ "Release", "Release", 0 ],
			"obj-15::obj-12::obj-21" : [ "time[20]", "time[20]", 0 ],
			"obj-15::obj-12::obj-46" : [ "time[19]", "time[1]", 0 ],
			"obj-15::obj-3::obj-40" : [ "live.text[12]", "live.text[4]", 0 ],
			"obj-6::obj-8::obj-40" : [ "Oversampling", "Oversampling", 0 ],
			"obj-6::obj-51::obj-28" : [ "live.tab[28]", "live.tab", 0 ],
			"obj-13" : [ "live.gain~[1]", "master", 0 ],
			"obj-15::obj-11::obj-62" : [ "time[13]", "time[13]", 0 ],
			"obj-15::obj-11::obj-31" : [ "live.text[8]", "live.text[4]", 0 ],
			"obj-15::obj-3::obj-62" : [ "time[50]", "time[29]", 0 ],
			"obj-15::obj-11::obj-48" : [ "time[3]", "time[3]", 0 ],
			"obj-15::obj-11::obj-57" : [ "time[8]", "time[8]", 0 ],
			"obj-15::obj-13::obj-61" : [ "time[35]", "time[28]", 0 ],
			"obj-42::obj-39" : [ "live.text[10]", "live.text", 0 ],
			"obj-6::obj-8::obj-52" : [ "RemixMode", "Mode", 1 ],
			"obj-6::obj-56::obj-28" : [ "live.tab[30]", "live.tab", 0 ],
			"obj-15::obj-13::obj-56" : [ "time[40]", "time[23]", 0 ],
			"obj-6::obj-8::obj-71" : [ "TranspMode", "TranspMode", 0 ],
			"obj-7::obj-4::obj-49" : [ "live.numbox[13]", "live.numbox", 0 ],
			"obj-15::obj-13::obj-47" : [ "time[45]", "time[2]", 0 ],
			"obj-42::obj-4::obj-36" : [ "live.numbox[9]", "live.numbox", 0 ],
			"obj-42::obj-29" : [ "live.numbox[19]", "live.numbox", 0 ],
			"obj-15::obj-13::obj-42" : [ "live.tab[24]", "live.tab[5]", 0 ],
			"obj-7::obj-12" : [ "Scrub", "Scrub", 0 ],
			"obj-15::obj-3::obj-60" : [ "time[52]", "time[27]", 0 ],
			"obj-6::obj-43" : [ "Delay[4]", "Delay", 0 ],
			"obj-8::obj-48" : [ "Attack", "Attack", 0 ],
			"obj-15::obj-12::obj-40" : [ "live.text[7]", "live.text[4]", 0 ],
			"obj-15::obj-12::obj-58" : [ "time[25]", "time[25]", 0 ],
			"obj-8::obj-77" : [ "OnsetVar", "OnsetVar", 0 ],
			"obj-8::obj-79" : [ "PitchTable", "PitchTable", 0 ],
			"obj-15::obj-3::obj-27" : [ "live.tab[34]", "live.tab[11]", 0 ],
			"obj-15::obj-3::obj-53" : [ "time[57]", "time[22]", 0 ],
			"obj-8::obj-38" : [ "Stretch", "Rate", 0 ],
			"obj-15::obj-3::obj-48" : [ "time[60]", "time[3]", 0 ],
			"obj-6::obj-8::obj-62" : [ "EnvScaleEnable", "Enable", 0 ],
			"obj-6::obj-51::obj-48" : [ "SpecEnv[1]", "EnvPres", 0 ],
			"obj-6::obj-8::obj-42" : [ "PreserveStereo", "Stereo", 1 ],
			"obj-15::obj-11::obj-21" : [ "time[0]", "time[0]", 0 ],
			"obj-15::obj-3::obj-64" : [ "time[48]", "time[31]", 0 ],
			"obj-15::obj-11::obj-63" : [ "time[14]", "time[14]", 0 ],
			"obj-15::obj-11::obj-40" : [ "live.text[9]", "live.text[4]", 0 ],
			"obj-15::obj-12::obj-62" : [ "time[29]", "time[29]", 0 ],
			"obj-15::obj-13::obj-27" : [ "live.tab[23]", "live.tab[11]", 0 ],
			"obj-8::obj-83" : [ "InitPitchTable[1]", "Init", 0 ],
			"obj-15::obj-11::obj-27" : [ "live.tab[21]", "live.tab[11]", 0 ],
			"obj-15::obj-11::obj-49" : [ "time[4]", "time[4]", 0 ],
			"obj-15::obj-13::obj-31" : [ "live.text[5]", "live.text[4]", 0 ],
			"obj-42::obj-27" : [ "live.numbox[27]", "live.numbox", 0 ],
			"obj-7::obj-68::obj-28" : [ "live.tab[25]", "live.tab", 0 ],
			"obj-15::obj-11::obj-58" : [ "time[9]", "time[9]", 0 ],
			"obj-15::obj-13::obj-60" : [ "time[36]", "time[27]", 0 ],
			"obj-6::obj-8::obj-50" : [ "RemixNoise", "Noise", 0 ],
			"obj-7::obj-33::obj-36" : [ "live.numbox[5]", "live.numbox", 0 ],
			"obj-15::obj-13::obj-53" : [ "time[41]", "time[22]", 0 ],
			"obj-6::obj-8::obj-38" : [ "WindowSize", "WindowSize", 0 ],
			"obj-7::obj-4::obj-28" : [ "live.tab[39]", "live.tab", 0 ],
			"obj-15::obj-13::obj-46" : [ "time[46]", "time[1]", 0 ],
			"obj-42::obj-4::obj-49" : [ "live.numbox[8]", "live.numbox", 0 ],
			"obj-6::obj-14" : [ "SpecEnv[2]", "ArpSwitch", 0 ],
			"obj-8::obj-31" : [ "Play", "Play", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "zp.harm2.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "zp.HarmTransEngine~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "zp.HarmTransVoice3~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "M4L.vdelay~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "M4L.pan1~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "M4L.gain1~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "M4L.bal2~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "resynth_controls.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "modulate-pan.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "modulate_freq_harm.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "freezer-scrubber.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "modulate_freq.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "freezer_voice~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "pan2S.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "thru.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "zp.sogs~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "freezer-interface.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "sogs-interface.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "harm-interface.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bend-interface.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "benderEngine~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "envfol.gendsp",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/code",
				"type" : "gDSP",
				"implicit" : 1
			}
, 			{
				"name" : "benderVoice~.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "fuzzy%.maxpat",
				"bootpath" : "~/Documents/Max 7/Projects/zp.acoustic_processor/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "supervp.trans~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "Lpad.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "supervp.ring~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "sogs~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mapper.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "supervp.scrub~.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "monacy",
				"default" : 				{
					"fontsize" : [ 9.0 ],
					"fontname" : [ "Monaco" ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
