{
	"name" : "zp.acoustic_processor",
	"version" : 1,
	"creationdate" : -767565146,
	"modificationdate" : -735464767,
	"viewrect" : [ 0.0, 45.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"acoustic_processor_top.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"zp.harm2.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"zp.HarmTransVoice3~.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"resynth_controls.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"modulate-pan.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"modulate_freq_harm.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"freezer-scrubber.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"modulate_freq.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"freezer_voice~.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"zp.sogs~.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"freezer-interface.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"sogs-interface.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"harm-interface.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"bend-interface.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"benderEngine~.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"benderVoice~.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"zp.harm.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"zp.HarmTransVoice.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"bender.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"code" : 		{

		}
,
		"data" : 		{
			"freeze.storage.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"harm.storage.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"bend.storage.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"sogs.storage.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"zp.ap.storage.json" : 			{
				"kind" : "json",
				"local" : 1
			}

		}
,
		"externals" : 		{

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : "."
}
